<?php 
    global $questions, $choices, $answers;

    $questions = array(
        "q1"=> array("Thiết bị nào sau đây dùng để kết nối mạng?", array(
            "choice1"=>"RAM",
            "choice2"=>"ROM",
            "choice3"=>"Router",
            "choice4"=>"CPU",
        )),
        "q2"=> array("Hệ thống nhớ của máy tính bao gồm:", array(
            "choice1"=>"Bộ nhớ trong, Bộ nhớ ngoài",
            "choice2"=>"Cache, Bộ nhớ ngoài",
            "choice3"=>"Bộ nhớ ngoài, ROM",
            "choice4"=>"Đĩa quang, Bộ nhớ trong",
        )),
        "q3"=> array("Trong mạng máy tính, thuật ngữ Share có ý nghĩa gì?", array(
            "choice1"=>"Chia sẻ tài nguyên",
            "choice2"=>"Nhãn hiệu của một thiết bị kết nối mạng",
            "choice3"=>"Thực hiện lệnh in trong mạng cục bộ",
            "choice4"=>"Một phần mềm hỗ trợ sử dụng mạng cục bộ",
        )),
        "q4"=> array("Bộ nhớ RAM và ROM là bộ nhớ gì?", array(
            "choice1"=>"Primary memory",
            "choice2"=>"Receive memory",
            "choice3"=>"Secondary memory",
            "choice4"=>"Random access memory",
        )),
        "q5"=> array("Các thiết bị nào thông dụng nhất hiện nay dùng để cung cấp dữ liệu cho máy xử lý? ", array(
            "choice1"=>"Bàn phím (Keyboard), Chuột (Mouse), Máy in (Printer)",
            "choice2"=>"Máy quét ảnh (Scaner)",
            "choice3"=>"Bàn phím (Keyboard), Chuột (Mouse) và Máy quét ảnh (Scaner)",
            "choice4"=>"Máy quét ảnh (Scaner), Chuột (Mouse) ",
        )),
        "q6"=> array("Khái niệm hệ điều hành là gì ? ", array(
            "choice1"=>"Cung cấp và xử lý các phần cứng và phần mềm",
            "choice2"=>"Nghiên cứu phương pháp, kỹ thuật xử lý thông tin bằng máy tính điện tử",
            "choice3"=>"Nghiên cứu về công nghệ phần cứng và phần mềm",
            "choice4"=>"Là một phần mềm chạy trên máy tính, dùng để điều hành, quản lý các thiết bị phần cứng và các tài nguyên phần mềm trên máy tính",
        )),
        "q7"=> array("Cho biết cách xóa một tập tin hay thư mục mà không di chuyển vào Recycle Bin:?", array(
            "choice1"=>"Chọn thư mục hay tâp tin cần xóa -> Delete",
            "choice2"=>"Chọn thư mục hay tâp tin cần xóa -> Ctrl + Delete",
            "choice3"=>"Chọn thư mục hay tâp tin cần xóa -> Alt + Delete",
            "choice4"=>"Chọn thư mục hay tâp tin cần xóa -> Shift + Delete",
        )),
        "q8"=> array("Danh sách các mục chọn trong thực đơn gọi là :", array(
            "choice1"=>"Menu pad",
            "choice2"=>"Menu options",
            "choice3"=>"Menu bar",
            "choice4"=>"Tất cả đều sai",
        )),
        "q9"=> array("Công dụng của phím Print Screen là gì?", array(
            "choice1"=>"In màn hình hiện hành ra máy in",
            "choice2"=>"Không có công dụng gì khi sử dụng 1 mình nó",
            "choice3"=>"In văn bản hiện hành ra máy in",
            "choice4"=>"Chụp màn hình hiện hành",
        )),
        "q10"=> array("Nếu bạn muốn làm cho cửa sổ nhỏ hơn (không kín màn hình), bạn nên sử dụng nút nào?", array(
            "choice1"=>"Maximum ",
            "choice2"=>"Minimum ",
            "choice3"=>"Restore down ",
            "choice4"=>"Close ",
        )),
    );

    $answers = array(
        "ans1"=>"c",
        "ans2"=>"a",
        "ans3"=>"a",
        "ans4"=>"a",
        "ans5"=>"c",
        "ans6"=>"d",
        "ans7"=>"d",
        "ans8"=>"c",
        "ans9"=>"d",
        "ans10"=>"c",
    );

?>