<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../styles/style.css">
</head>
<body>
    <?php
        include ("../data.php");
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $result = $_COOKIE["result"];
            if($_POST['submit']) {
                $count = 0;
                foreach ($questions as $key=>$value) {
                    $count++;
                    if ($count < 6) continue;
                    if(isset($_POST["$key"]) && !is_null($_POST["$key"])){
                        if ($_POST["$key"] == $answers["ans".$count]) {
                            $result++;
                        }
                    }
                }  
            }
            setcookie("result", $result, time() + 300, "/");
            header("location: ./result.php");
        }
    ?>

    <form action="" method="post">
        <div class="center flex-center">
            <div class="container">
                <div class="mt-20">
                    <?php 
                        $count = 0;
                        foreach ($questions as $quest=>$value) {
                            $count++;
                            if ($count<6) continue;
                            echo '<div class="mt-10">
                                    <label for=""> Câu '.$count.': '.$value[0].'</label><br>
                                    <input type="radio" name="'.$quest.'" value="a">'.$value[1]["choice1"].'<br>
                                    <input type="radio" name="'.$quest.'" value="b">'.$value[1]["choice2"].'<br>
                                    <input type="radio" name="'.$quest.'" value="c">'.$value[1]["choice3"].'<br>
                                    <input type="radio" name="'.$quest.'" value="d">'.$value[1]["choice4"].'<br>
                                </div>';
                        }
                    ?>
                </div>
                <div class="mt-20">
                    <input type="submit" name="submit" value="Nộp bài">
                </div>
            </div>
        </div>
    </form>
</body>
</html>